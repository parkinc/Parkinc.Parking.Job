﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EasyNetQ;
using Parkinc.Parking.Messages;

namespace Parkinc.Parking.Job
{
    public class Requester
    {
        private readonly IBus _bus;
        private readonly CancellationTokenSource _cts = new CancellationTokenSource();

        public Requester(IBus bus)
        {
            _bus = bus;
        }

        public async void Start(TimeSpan interval)
        {
            while (!_cts.IsCancellationRequested)
            {
                var msg = new ParkingDataRequestV1();
                await _bus.PublishAsync(msg);
                Console.WriteLine($"[{DateTime.Now:G}] Request published");
                await Task.Delay(interval, _cts.Token);
            }
        }

        public void Stop()
        {
            _cts.Cancel();
        }
    }
}
