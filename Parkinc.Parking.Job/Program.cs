﻿using System;
using System.IO;
using System.Runtime.Loader;
using EasyNetQ;
using Microsoft.Extensions.Configuration;

namespace Parkinc.Parking.Job
{
    class Program
    {
        static void Main(string[] args)
        {
            var cfg = GetConfig();
            var bus = RabbitHutch.CreateBus(cfg["Messaging:ConnectionString"]);

            var requester = new Requester(bus);
            requester.Start(TimeSpan.FromSeconds(5));

            AssemblyLoadContext.Default.Unloading += context => requester.Stop();
        }

        private static IConfiguration GetConfig()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            return builder.Build();
        }
    }
}
